const request = require('supertest');
const app = require('../app');

describe('GET /', () => {
  it('responds with JSON containing "Hello World!"', async () => {
    const response = await request(app).get('/');
    expect(response.statusCode).toBe(200);
    expect(response.body.message).toBe('Hello World!');
  });

  it('responds with status code 404 for non-existing route', async () => {
    const response = await request(app).get('/non-existing');
    expect(response.statusCode).toBe(404);
  });

  it('responds with JSON containing the correct content type', async () => {
    const response = await request(app).get('/');
    expect(response.headers['content-type']).toContain('application/json');
  });

  it('responds with JSON containing the expected message property', async () => {
    const response = await request(app).get('/');
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Hello World!');
  });

  it('responds with JSON containing "Hello World!" and not "Bye World!"', async () => {
    const response = await request(app).get('/');
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('message');
    expect(response.body.message).toBe('Hello World!');
    expect(response.body.message).not.toBe('Bye World!');
  });
});
