
## Task-Tracking

### Alternative 1: Asana

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |       10 |
| Funktionen                | 25%      |        9 |
| Integration               | 20%      |        9 |
| Anpassbarkeit             | 20%      |        8 |
| Kosten                    | 15%      |        8 |
| **Gesamtbewertung**       | **100%** | **8.8**  |


### Alternative 2: Trello

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |        9 |
| Funktionen                | 25%      |       10 |
| Integration               | 20%      |       10 |
| Anpassbarkeit             | 20%      |        9 |
| Kosten                    | 15%      |        7 |
| **Gesamtbewertung**       | **100%** | **9.0**  |


### Alternative 3: Monday.com

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |        8 |
| Funktionen                | 25%      |        9 |
| Integration               | 20%      |        8 |
| Anpassbarkeit             | 20%      |       10 |
| Kosten                    | 15%      |        7 |
| **Gesamtbewertung**       | **100%** | **8.6**  |


## Empfehlung

Basierend auf der Bewertungsmatrix und den oben genannten Kriterien empfehlen wir folgende Tools:

Für die Teamkommunikation:

1. Microsoft Teams
2. Slack
3. Google Meet

Microsoft Teams erhält aufgrund seiner Verfügbarkeit auf allen notwendigen Plattformen, der umfangreichen Integration mit anderen Microsoft-Tools wie Outlook und SharePoint, der hohen Sicherheit und der breiten Palette an Funktionen wie Video- und Audiokonferenzen, Bildschirmfreigabe und Dateiübertragung die höchste Bewertung.

Für das Task-Tracking:

1. Trello
2. Asana
3. Monday.com

Trello erhält aufgrund seiner Benutzerfreundlichkeit, der einfachen Anpassbarkeit an spezifische Anforderungen, der Verfügbarkeit auf verschiedenen Plattformen und der günstigen Kosten die höchste Bewertung.

Wir empfehlen, Microsoft Teams und Trello als Tools zur Unterstützung der Teamarbeit zu verwenden. Beide Tools bieten umfangreiche Funktionen, sind benutzerfreundlich und können nahtlos integriert werden.
