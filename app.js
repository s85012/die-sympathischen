const express = require('express');
const app = express();
const port = process.env.PORT || 80;

app.get('/', (req, res) => {
  const message = { message: 'Hello World!' };
  res.json(message);
});

app.listen(port, () => {
  console.log('Server running on port 80');
});

module.exports = app;
