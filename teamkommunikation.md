# Team-Tools Bewertungsmatrix

## Team-Kommunikation

### Alternative 1: Microsoft Teams

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |        9 |
| Funktionen                | 15%      |        8 |
| Integration               | 25%      |       10 |
| Anpassbarkeit             | 20%      |        9 |
| Kosten                    | 20%      |        8 |
| **Gesamtbewertung**       | **100%** | **8.9**  |


### Alternative 2: Slack

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |        8 |
| Funktionen                | 15%      |        9 |
| Integration               | 25%      |        8 |
| Anpassbarkeit             | 20%      |       10 |
| Kosten                    | 20%      |        9 |
| **Gesamtbewertung**       | **100%** | **8.8**  |


### Alternative 3: Google Meet

| Kriterium             |  Gewichtung  | Bewertung |
| ----------------------|:------------:|:---------:|
| Benutzerfreundlichkeit    | 20%      |        9 |
| Funktionen                | 15%      |        7 |
| Integration               | 25%      |        7 |
| Anpassbarkeit             | 20%      |        8 |
| Kosten                    | 15%      |        9 |
| **Gesamtbewertung**       | **100%** | **7.9**  |
